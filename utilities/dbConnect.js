var mysql = require('mysql');
var pool = null
var ConnectToDB = function(){ 
}

ConnectToDB.prototype.connectionInitializer = function(hostname,user, password, database){
	pool = mysql.createPool({
	   connectionLimit : 10,
	  host:hostname,
	  user:user,
	  password:password,
	  database: database
	});
	 
	return pool;
}
ConnectToDB.prototype.endConnection = function(){
	pool.end();
}

module.exports = ConnectToDB; 

