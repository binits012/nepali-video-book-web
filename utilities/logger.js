var path = require("path");
var winston = require('winston');

var logger = new winston.Logger({
    
 });
 logger.configure({
    level: 'info',
    transports: [
      new (require('winston-daily-rotate-file'))({ name: 'file§',
  datePattern: '.yyyy-MM-dd',
	  filename: '/home/NepaliVideoBook/logs/nepaliVideoBook-log.txt',
            maxsize: 1024 * 1024 * 10}) // 10MB})
    ]
  }); 

module.exports = logger;
