var express = require('express');
var router = express.Router();
var moment = require('moment');

var async = require('async');
var dbConnect = require('../utilities/dbConnect');
var db = new dbConnect();
var pool = db.connectionInitializer('localhost', 'root', 'binit2016*', "nepalivideobook");
var logger = require('../utilities/logger');

router.get('/', function(req, res){

	if(req.session.name){
		var langInsertSql = "select * from language;";

		pool.getConnection(function(err, connection){

			var langResult = connection.query(langInsertSql, function(err, myRows){
				if(!err){
					var channelSql = "select channels_pid, ch.name, imageUrl, language_fid, counter, language_pid, ln.name as lng from channels as ch, language as ln where language_fid = language_pid;";
					connection.query(channelSql, function(err, chRows){
						if(!err){
							var categoriesSql = "select categories_pid,  cat.name, channel_fid, cat.counter, ch.name as channelName,ch.channels_pid from nepalivideobook.categories as cat, channels as ch where cat.channel_fid = ch.channels_pid;"
							connection.query(categoriesSql, function(err, cateRows){
								if(!err){
									res.render('entries',{title:"Entries", user:req.session.name.name, langData:myRows, channelData:chRows, categoryData:cateRows});
								}else{
									throw err;
								}
							});
							
						}
					});
					

				}else{
					throw err;
				}
			});
			connection.release();
		});
		
	}else{
		res.redirect('/login');
	}
});



router.post('/addLang', function(req,res){

	if(req.session.name){
		var langName = req.body.langName;

		if(typeof langName != null || typeof langName != "undefined"){
			var langInsertSql = "insert into  language (name) value (?) ;"
			pool.getConnection(function(err, connection){
				var passGen = connection.query(langInsertSql, (langName), function(err, myRows){
					if(!err){
						logger.info(myRows);
						res.redirect('/entries?added=true');
					}else{
						res.redirect('/entries?error=true');
					}
				});
				connection.release();
			});

		}else{
			res.redirect('/entries?error=true');
		}
	}else{
		res.redirect('/login');
	}
});


router.post('/addChannel', function(req, res){

	if(req.session.name){

		var lang = req.body.lang.trim();
		var name = req.body.channelName.trim();
		var imageUrl = req.body.imageUrl.trim();
		logger.info(lang + name + imageUrl);
		if(typeof lang != null && typeof name != null){
			var channelSql = "insert into channels  set ?";
			var values = {
				name:name,
				imageUrl:imageUrl,
				language_fid:lang
			}
			pool.getConnection(function(err, connection){
				var channelResult = connection.query(channelSql, values, function(err, myRows){
					if(!err){
						res.redirect('/entries?added=true');
					}else{
						logger.info(err);
						res.redirect('/entries?error=true');
					}
				});
				connection.release();
			});
			
		} 

	}else{
		res.redirect('/login');
	}
});


router.post('/addCategory', function(req, res){

	if(req.session.name){
		var channel = req.body.channel;
		var catName = req.body.catName;
		if(typeof channel != null && typeof catName != null){
			var values = {
				name:catName,
				channel_fid:channel
			};
			var categoryInsertSql = "insert into  categories set ?;"
			pool.getConnection(function(err, connection){
				var passGen = connection.query(categoryInsertSql, values , function(err, myRows){
					if(!err){
						logger.info(myRows);
						res.redirect('/entries?added=true');
					}
					else
					{
						res.redirect('/entries?error=true');
					}
				});
				connection.release();
			});
			 
		}else{
			res.redirect('/entries?error=true');
		}

	}else{

	}
});


// entries, user clicekd on 
router.get('/channel/:channelId/:categoryId', function(req, res){

	logger.info(" Category Called ");
	if(req.session.name){
		var channelId = req.params.channelId;
		var categoryId = req.params.categoryId;

		if(typeof channelId != null && typeof categoryId != null){
			
				var channelSql = "select channels_pid, cat.name as CategoryName, imageUrl, language_fid, cat.counter, categories_pid, ch.name as channelName, channel_fid, ch.counter from  categories as cat, channels as ch where channels_pid =? and categories_pid=?"
				pool.getConnection(function(err, connection){
					var channelResult = connection.query(channelSql, [channelId, categoryId ] , function(err, myRows){
						if(!err){
							//logger.info(myRows);
							var albumSql = "select * from album where category_fid=? order by name";
							var albumResult = connection.query(albumSql, categoryId, function(err, albumRows){
								logger.info('Album Data');
								logger.info(albumRows);
								if(!err){
									res.render('albums', {title:"Albums",  user:req.session.name.name, ChNCat:myRows, albumData:albumRows});
								}else{
									throw err;
								}
							});
							
						}else{ 
							logger.info(err);
						}
				});

				logger.info(channelResult.sql);	
				connection.release();
			});

			
		}
		else {
			logger.info();
		}
	}
	else
	{
		res.redirect('/login');

	}

});

router.get('/albums/:albumId', function(req,res){
	
	if(req.session.name){
		var albumSql = "select * from album where album_pid =?;"
		var albumId = req.params.albumId;
		logger.info('albums selected' + albumId);
		if(typeof albumId != null){

			pool.getConnection(function(err, connection){
				var albumResult = connection.query(albumSql, albumId, function(err, myRows){
					if(!err){
						var  episodeSql = "select * from episode where album_fid=?";
						var episodeData = connection.query(episodeSql, albumId, function(err, epData){
							if(!err){
								res.render('album', {title:"Album",  user:req.session.name.name, albumData:myRows, episodeData:epData});
							}else{
								logger.info(err);
							}
						});
						
					}
					else{
						logger.info(err);
					}
				});

				connection.release();
			});

		}else{
			logger.info('albums selected' + albumId);
			res.redirect('/login');
		}
		
		
	}else{
		res.redirect('/login');
	}
});



router.get('/albums/selectedAlbum/:epId', function(req,res){
	if(req.session.name){
		var episodeId = req.params.epId;
		if(typeof episodeId != null ){
			pool.getConnection(function(err, connection){
				if(!err){
					var episodeSql = "select * from episode where episode_pid=?";
					var episodeResult = connection.query(episodeSql, episodeId, function(err, myRows){

						if(!err){
							var linkSql = "select * from links where episode_fid =?";
							var linkResult =  connection.query(linkSql,episodeId, function(err, linkRows){

								if(!err){
									res.render('links', {title:"Links",  user:req.session.name.name, episodeData:myRows, linkData:linkRows});
								}
							});
						}
					});
				}else{
					throw err;
				}
				connection.release();

			});
		}
		
	}else{
		res.redirect('/login');
	}
});




// from here we handle the post
// adding the links to the selected episodes
router.post('/addlinks', function(req, res){
	if(req.session.name){
		var position = req.body.position;
		var linksUrl = req.body.linksUrl;
		var episodeId = req.body.episodeId;

		if(typeof position != null && typeof linksUrl != null && typeof episodeId != null){
			logger.info(position + linksUrl + episodeId);
			pool.getConnection(function(err, connection){
				if(!err){
					var linkInsertSql = "insert into links set ? ";
					var values = { url : linksUrl, episode_fid:episodeId, position:position };
					var linkInsert = connection.query(linkInsertSql, values, function(err, myRows){
						if(!err){
							res.redirect('/entries/albums/selectedAlbum/'+episodeId+"?added=true");
						}else{
							logger.info(err);
							res.redirect('/entries/albums/selectedAlbum/'+episodeId+"?error=true");
						}
					});

				}else{

					throw err;
				}
				connection.release();
			});
			
		}else{
			res.redirect('/entries/albums/selectedAlbum/'+episodeId+"?error=true");
		}

	}else{
		res.redirect('/login');
	}
});



router.post('/editlinks', function(req, res){
	if(req.session.name){
		var position = req.body.editPosition;
		var linksUrl = req.body.editLinksUrl;
		var linkId = req.body.linkId;
		var episodeId = req.body.episodeId;

		if(typeof position != null && typeof linksUrl != null && typeof linkId != null){
			logger.info(" edit links " + position + linksUrl + linkId);
			pool.getConnection(function(err, connection){
				if(!err){
					var linkUpdateSql = "update links set ? where links_pid=?";
					var values = { url : linksUrl, episode_fid:episodeId, position:position };
					var updateResult = connection.query(linkUpdateSql, [values, linkId], function(err, myRows){
						if(!err){
							res.redirect('/entries/albums/selectedAlbum/'+episodeId+"?edited=true");
						}else{
							res.redirect('/entries/albums/selectedAlbum/'+episodeId+"?error=true");
						}
					});
				 

				}else{

					throw err;
				}
				connection.release();
			});
			
		}else{
			res.redirect('/entries/albums/selectedAlbum/'+episodeId+"?error=true");
		}

	}else{
		res.redirect('/login');
	}
});

// adding new program to category
router.post('/addProgram', function(req, res){
	if(req.session.name){
		var program = req.body.program;
		var img = req.body.img;
		var description = req.body.description;
		var categoryId = req.body.categoryId;
		var channelId = req.body.channelId;

		if(typeof program != null && typeof img != null && typeof description != null && typeof categoryId != null){
			pool.getConnection(function(err, connection){
				var addProgramSql = "insert into album set ?";
				var values = {
					name:program,
					description:description,
					imgUrl: img,
					category_fid: categoryId 
				}

				if(!err){
					var addProgramResult = connection.query(addProgramSql, values, function(err, myRows){
						if(!err){
							res.redirect("/entries/channel/"+channelId+"/"+ categoryId+"?added=true");
						}else{
							logger.info(err);
							res.redirect("/entries/channel/"+channelId+"/"+ categoryId+"?error=true");
						}
					} );
				}else{
					logger.info(err);
					res.redirect("/entries/channel/"+channelId+"/"+ categoryId+"?error=true");
				}

				connection.release();	
			});
		}
	}else{
		res.redirect('/login');
	}
});

//editing program
router.post('/editProgram', function(req,res){
	if(req.session.name){
		var program = req.body.program;
		var img = req.body.img;
		var description = req.body.description;
		var categoryId = req.body.categoryId;
		var channelId = req.body.channelId;
		var albumId = req.body.albumId;
		logger.info( "*************** edit program ***************");
		logger.info(program + img + description + categoryId + channelId);
		if(req.session.name){
			if(typeof program != null && typeof img != null && typeof description != null && typeof categoryId != null){
				//logger.info(program + img + description + categoryId + channelId);
				pool.getConnection(function(err, connection){
					if(err){

					}else{
						var updateProgramSql = "update album set ? where album_pid =?";
						var values = {
							name: program,
							description: description,
							imgUrl: img
						};
						var insertUpdateProgramResult = connection.query(updateProgramSql, [values,albumId], function(err, myRows){
							if(!err){
								res.redirect("/entries/channel/"+channelId+"/"+ categoryId+"?added=true");
							}else{
								logger.info(err);
								res.redirect("/entries/channel/"+channelId+"/"+ categoryId+"?error=true");
							}
						});
						logger.info(insertUpdateProgramResult.sql);

					}

					connection.release();
				});
				

			}
		}
		else{
			logger.info('')
			res.redirect("/entries/channel/"+channelId+"/"+ categoryId+"?error=true");
		}
		
	}else{
		res.redirect('/login');
	}
});

// adding new episode to album
router.post('/addEpisode', function(req, res){
	if(req.session.name){
		var albumId = req.body.albumId;
		var program = req.body.program;
		var description = req.body.description;
		if(typeof description == null || typeof description == "undefined"){
			description=" ";
		}


		if(typeof albumId != null && typeof program != null ){

			pool.getConnection(function(err, connection){
				if(!err){
					var episodeInsertSql = "insert into episode set ?";
					var values = {
						name: program,
						description: description,
						album_fid: albumId
					};

					var insertEpisdes = connection.query(episodeInsertSql, values, function(err, myRows){
						if(!err){
							res.redirect("/entries/albums/"+ albumId +"?added=true");
						}else{
							res.redirect("/entries/albums/"+ albumId +"?error=true");
						}
					});

				}
				connection.release();
			});
			
		}else{
			res.redirect("/entries/albums/"+ albumId +"?error=true");
		}

	}else{
		
	}
});

router.post('/editEpisode', function(req, res){
	if(req.session.name){
		 var program = req.body.program;
		 var description = req.body.description;
		 var albumId = req.body.albumId;
		 var episodeId = req.body.episodeId;

		 if(typeof program != "undefined" && typeof description != "undefined" && typeof albumId != "undefined" 
		 	&& typeof episodeId != "undefined"){
		 	pool.getConnection(function(err, connection){
		 		if(!err){
		 			var updateSql = "update episode set ? where episode_pid =?";
		 			var values = {
		 				name:program,
		 				description:description
		 			};
		 			var updateResult = connection.query(updateSql, [values, episodeId], function(err, myRows){
		 				if(!err){
		 					res.redirect("/entries/albums/"+ albumId +"?true=true");
		 				}else{
		 					res.redirect("/entries/albums/"+ albumId +"?error=true");
		 				}
		 			});
		 			logger.info(updateResult.sql);
		 		}else{
		 			res.redirect("/entries/albums/"+ albumId +"?error=true");
		 		}
		 		connection.release();
		 	});
		 }else{
		 	res.redirect("/entries/albums/"+ albumId +"?error=true");
		 }

		
	}else{
		res.redirect('/login');
	}
});

module.exports = router;
