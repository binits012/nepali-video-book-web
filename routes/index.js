var express = require('express');
var router = express.Router();
var logger = require('../utilities/logger');
var async = require('async');
var dbConnect = require('../utilities/dbConnect');
var db = new dbConnect();
var pool = db.connectionInitializer('localhost', 'root', 'binit2016*', "nepalivideobook"); 

/* GET home page. */
 var clearSession = function(session, callback){
  session.destroy();
  callback();
};
router.get('/logout', function(req,res){
  clearSession(req.session, function () {
    res.redirect('/login');;
  });

 });


router.get('/', function(req,res){
	
	pool.getConnection(function(err, connection){
		if(!err){ 
			var langNChSql = "select * from  language "; 
			var recentlyUpdatedEpisdoeSql = "select l.language_pid, l.name as lang, c.channels_pid, c.name as channel, c.language_fid, cat.categories_pid , cat.name as category, cat.channel_fid,  episode_pid, e.name as epiName, e.description as epDesc, e.createdAt, e.img, album_fid, e.counter as epCounter, e.publish, album_pid, a.name, a.imgUrl, a.category_fid from language as l, channels as c, categories as cat, episode as e, album as a where l.language_pid= c.language_fid and c.channels_pid=cat.channel_fid and cat.categories_pid = a.category_fid and album_pid=album_fid order by createdAt desc limit 40;"
			var lanNChResult = connection.query(langNChSql, function(err, myRows){
				if(!err){
					var recentlyUpdateResult = connection.query(recentlyUpdatedEpisdoeSql, function(err, rData){

						if(!err){
							res.render('first',{title:"Nepali Video Book: Watch your favourite serials. ", lngData:myRows, recenlyUpdateData:rData});
						}else{
							logger.info(err);
						}
					});
					logger.info(recentlyUpdateResult.sql);
				}else{

				}
			});


		}else{
			logger.info(err);
		}
		connection.release();
	});
});

router.get('/lang/:langId', function(req, res){
	var id = req.params.langId;
	pool.getConnection(function(err, connection){
		if(!err){ 
			var langNChSql = "select * from  language "; 
			var langResult = connection.query(langNChSql,  function(err, myRows){
				var channelSql = "select * from channels where language_fid=? order by name";
				var channelResult = connection.query(channelSql, id, function(err, chRows){
					if(!err){
						var recentlyUpdatedSql = "select c.name as channelName, c.channels_pid, c.language_fid, cat.categories_pid, cat.name as catName, cat.channel_fid, a.album_pid, a.name as albumName, a.category_fid, a.imgUrl, e.episode_pid, e.name as epiName, e.album_fid,  e.createdAt  from channels as c join categories as cat on cat.channel_fid = c.channels_pid join album as a on a.category_fid = cat.categories_pid join episode as e on e.album_fid = a.album_pid where c.language_fid =? order by e.createdAt  desc limit 40"
						var recentlyUpdateResult = connection.query(recentlyUpdatedSql, id, function(err, rData){
							if(!err){
								logger.info(rData);
								res.render('lngSelected',{title:"Nepali Video Book: Watch your favourite serials. ", lngData:myRows, channelData:chRows, recenlyUpdateData:rData});
							}
						});
						
					}
				});
			});

		}else{
			logger.info(err);
		}
		connection.release();
	});



});


//user clicked on episode

router.get('/viewEpisodes/:epId', function(req, res){
	var epId = req.params.epId;
	logger.info(epId);
	if (typeof parseInt(epId) === 'number' ) {
	    if(typeof epId != null && typeof epId != "undefined"){
		  pool.getConnection(function(err, connection){
			  if(!err){
				  var langNChSql = "select * from  language "; 
				  var lanNChResult = connection.query(langNChSql, function(err, lngData){
					  if(!err){
						  var sql = "select * from episode as e, links as l where e.episode_pid = l.episode_fid and e.episode_pid=? and l.status=1;"
						  var result = connection.query(sql, epId, function(err, myRows){
							  if(!err){
								  res.render('selectedEpisode', {title:"NVB", epData:myRows, lngData:lngData});
  
							  }
						  });
  
						  var selectSql = "select counter, episode_pid from episode where episode_pid =?";
						  var selectResult = connection.query(selectSql, epId, function(err, myRows){
							  if(!err){
								  console.log(myRows)
								  logger.info(myRows);
								  if (myRows.length > 0 ) {
								    var counter = parseInt(myRows[0].counter) +1;
								    var updateSql = "update episode set counter=? where episode_pid = ? ";
								    var updateResult = connection.query(updateSql,[counter, epId], function(err, upData){
									    if(!err){
										    logger.info(upData);
									    }
								    });
								  }
								  
								  //logger.info(updateResult.sql);
  
							  }
						  });
					  }
				  });
				  
			  }
			  connection.release();
		  });
	  }else{
		  res.render("error",{title:"ERROR"});
	  }
	}else{
	  res.render("error",{title:"ERROR"});
	}
	
	
});





// user clicked on the particular channel

router.get('/channel/:id', function(req,res){

	var id = req.params.id;
	if(typeof id != 'undefined' && typeof id != null){
		pool.getConnection(function(err, connection){
			if(!err){ 
				var langNChSql = "select * from  language "; 
				var lanNChResult = connection.query(langNChSql, function(err, lngData){
					if(!err){

						var channelSql = "select * from channels where channels_pid=?";
						 
						var channelResult = connection.query(channelSql, id, function(err, chRows){
							if(!err && chRows.length > 0){
								var channels = "select * from channels where language_fid=? order by name"
								var myChannels = connection.query(channels, chRows[0].language_fid, function(err, channelData){
								  if(!err){
								    var sql = "select c.categories_pid, c.name as catName, c.channel_fid, a.category_fid, a.album_pid, a.name as albumName, a.imgUrl, a.createdAt from categories as c join album as a on a.category_fid=c.categories_pid where c.channel_fid=? order by a.name ;";
								    var sqlResult = connection.query(sql, id, function(err, sqlRow){
									    if(!err){
										    res.render('chSelected',{title:"Nepali Video Book: Watch your favourite serials. ", lngData:lngData, channelData:channelData, recenlyUpdateData:sqlRow});
									    }
								    });   
								  }  
								}); 

							}else{
							 res.render("error",{title:"ERROR"}); 
							}


						});





						
					}

				});

			}
			connection.release();
		});	

	}
});


//user clicked on the album after selecting the channel
// we need to get all the episodes of the album in desc order

router.get('/album/:id', function(req, res){

	var id = req.params.id;
	if(typeof id != 'undefined' && typeof id != null){
		pool.getConnection(function(err, connection){
			if(!err){ 
				var langNChSql = "select * from  language "; 
				var lanNChResult = connection.query(langNChSql, function(err, lngData){
					if(!err){
						var albumSql = "select e.episode_pid, e.name as epiName, e.description as epDesc, e.img, e.createdAt, e.album_fid, e.counter as epCounter, e.publish, a.album_pid, a.name, a.imgUrl from episode as e join album as a on e.album_fid=a.album_pid  where e.album_fid=?  order by createdAt desc ";
						var albumResult = connection.query(albumSql, id, function(err, myRows){
							if(!err){
								res.render('first',{title:"Nepali Video Book: Watch your favourite serials. ", lngData:lngData, recenlyUpdateData:myRows})
							}
						});
					}
				});
			}
			connection.release();
		});
	}

});

router.get('/login', function(req, res) {

  logger.info(req.session.name);	
  if(typeof req.session.name === 'undefined'){

  	res.render('index', { title: 'Nepali Video Book' });
  	
  }
  else{

  	res.redirect('/dashboard');



  	 //res.render("dashboard",{"title":"Dhunga Khani", user:req.session.name.name});
  }
 
});

router.post('/login', function(req,res){

	var user = req.body.username;
	var password = req.body.password;

	logger.info(user + password);
	logger.info("user name = " + user);
	logger.info("password = " + password);
	if(typeof user != undefined && typeof user != null && typeof(password) != null && typeof(password) != undefined){
		if(typeof db === null || typeof db === undefined){
		    db = new dbConnect();
		    logger.info('db initialized');
		}

		var sql =  'select * from nepalividebook_web_users where username = ? and password = ? and status =1'; 
		pool.getConnection(function(err,connection){
		  	var q1 =  connection.query(sql, [user,password] , function(err,data1){
		  		logger.info(data1);
		  		if(!err){
		  			if(data1.length > 0){
		  				logger.info(data1);
		  				req.session.name = {'name':user};
		  				res.status(200).send({"reply":"100"});
		  			}else{
		  				logger.info("sending forbidden");
		  				res.status(200).send({"reply":"101"});
		  			}
		  		}
		  	});
		  	 connection.release();

		  	 logger.info("sql" +q1.sql);
		    // pool.release();
		});

	}else{
		res.render('index', { title: 'Dhunga Khani' });
	}
});

router.get('/dashboard', function(req, res){
	if(req.session.name){
		 
		var dhuwaniSql = "select  * from language;"
		pool.getConnection(function(err, connection){
			var dhuwani = connection.query(dhuwaniSql, function(err, myRows){
				if(!err){
					logger.info(myRows);
					//res.render("passGenerator",{title:"Pass Generator", user:req.session.name.name, passData:myRows});
					

					var incoming = 0;
					var outgoing = 0;
					var processing = 0;
					for(var i = 0; i< myRows.length; i++){
						if(myRows[i].dhuwaniStatus == 0 ){
							incoming++;
						}
						if(myRows[i].dhuwaniStatus == 2 ){
							outgoing++;
						}
						if(myRows[i].dhuwaniStatus == 1 ){
							processing++;
						}
						 
					}
					logger.info("incoming =" +incoming);
					res.render("dashboard",{title:"Dashboard", user:req.session.name.name, dhuwaniData:myRows, incoming:incoming,
					 processing:processing, outgoing:outgoing});
				}else{

				}
			});
			connection.release();
		});
	}else{
		res.redirect('/login');;
	}
});

//
router.get('/termsNconditions', function(req, res){
  res.render('termsncondition', {title:"NVB- terms and condition"});
});

module.exports = router;
