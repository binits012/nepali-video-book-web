var express = require('express');
var router = express.Router();
var moment = require('moment');
var logger = require('../utilities/logger');
var async = require('async');
var dbConnect = require('../utilities/dbConnect');
var db = new dbConnect();
var pool = db.connectionInitializer('localhost', 'root', '', "nepalivideobook");

router.get('/', function(req, res){
	if(req.session.name){
		if(typeof db === null || typeof db === undefined){
	    	db = new dbConnect();
	    	console.log('db initialized');
		}
		var passGeneratorSql = "select * from passGenerator ;"
		pool.getConnection(function(err, connection){
			var passGen = connection.query(passGeneratorSql, function(err, myRows){
				if(!err){
					console.log(myRows);
					res.render("passGenerator",{title:"Pass Generator", user:req.session.name.name, passData:myRows});
				}else{

				}
			});
			connection.release();
		});
		

		
	}else{
		res.redirect('/');
	}

});

router.post('/', function(req, res){

	if(req.session.name){
		var fname = req.body.passFname;
		var lname = req.body.passLname;
		var mobile = req.body.passMobile;

		if(typeof(fname) != "undefined" && typeof(lname) != "undefined" && typeof(mobile) != "undefined"){

			if(typeof db === null || typeof db === undefined){
		    	db = new dbConnect();
		    	console.log('db initialized');
			}

			var insertSql =  'INSERT INTO  passGenerator set ?'; 
			//INSERT INTO `MyDhungaKhani`.`passGenerator` (`firstName`, `lastName`, `mobileNumber`, `status`) VALUES ('Nidhi', 'Lakoul', '358442733404', '1');
			var checkSql = 'select * from passGenerator where mobileNumber=?';
			pool.getConnection(function(err,connection){
		  	var q1 =  connection.query(checkSql, [mobile] , function(err,data1){
		  		//console.log(data1);
		  		if(!err){
		  			if(data1.length > 0){
		  				logger.info("user already in the pass generator table =" + data1[0].passGenerator_pid);
		  				console.log("duplicate user");
		  				res.status(200).send({"reply":"105"});
		  			}else{
		  				var insertValue = {
		  					firstName:fname,
		  					lastName:lname,
		  					mobileNumber:mobile,
		  					status:1,
		  				};
		  				var insertPass = connection.query(insertSql, insertValue, function(err, myRow){
		  					if(err){
		  						console.log(err);
		  						res.status(200).send({"reply":"102"});
		  					}else{
		  						console.log('new user created');
		  						res.status(200).send({"reply":"100"});
		  					}	
		  				});
		  				 
		  			}
		  		}
		  	});
		  	 connection.release();

		  	 console.log("sql" +q1.sql);
		    // pool.release();
		});

		}else{
			res.status(200).send({"reply":"102"});
		}
		//res.status(200).send({"reply":"100"});
	}else{
		res.status(200).send({"reply":"101"});
	}
});


router.post('/edit', function(req,res){
	if(req.session.name){
		var passGenEditFname = req.body.passGenEditFname;
		var passGenEditLname = req.body.passGenEditLname;
		var passGenEditMobile = req.body.passGenEditMobile;
		var passGeneratorPid = req.body.passGeneratorPid;

		if(typeof(passGeneratorPid) != "undefined"){
			var data = {
				firstName:passGenEditFname,
				lastName:passGenEditLname,
				mobileNumber:passGenEditMobile
			};
			var editSql = 'update passGenerator set ? where passGenerator_pid=?';
			pool.getConnection(function(err,connection){
				var edit = connection.query(editSql, [data,passGeneratorPid], function(err, data){
					if(!err){
						res.status(200).send({"reply":"100"});
					}else{
						res.status(200).send({"reply":"102"});
					}
				});
				console.log(edit.sql);
				connection.release();
			});

		}

	}else{
		res.redirect('/');res.status(200).send({"reply":"101"});
	}

});


module.exports = router;