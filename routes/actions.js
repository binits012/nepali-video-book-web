(function(){ 
    var moment = require('moment');
    var async = require('async');
    var dbConnect = require('../utilities/dbConnect');
    var db = new dbConnect();
    var pool = db.connectionInitializer('localhost', 'root', 'binit2016*', "nepalivideobook"); 
 
    //select * from episode as e, links as l where e.episode_pid = l.episode_fid and l.status=0 and e.createdAt between curdate()-14 and CURDATE()-7 order by e.createdAt desc limit 10;
    //we gonna accept only and only sql
    
    var Action = (function(){
        function Action(sql){
            this.sql = sql;
            
        };
        Action.prototype.update = function(cb){
           
            console.log(this.sql);
            var sql = this.sql;
            pool.getConnection(function(err, connection){
                 
                if(!err){
                    
                    var lanNChResult = connection.query(sql, function(err, myRows){
                        console.log(err);
                        if(!err){
                            cb(myRows, null);		 
                        }else{
                            cb(  null, err);
                        }     
                           
                    });
        
        
                }else{
                    cb(null,err )
                        //logger.info(err);
                }
                connection.release();
            });
            
            
        };
        
        Action.prototype.episodeCounter = function(cb){
            
            console.log(this.sql);
            var sql = this.sql;
            pool.getConnection(function(err, connection){
                
                if(!err){
                    
                    var lanNChResult = connection.query(sql, function(err, myRows){
                        console.log(err);
                        if(!err){
                            cb(myRows, null);		 
                        }else{
                            cb(  null, err);
                        }     
                           
                    });
        
        
                }else{
                    cb(null,err )
                        //logger.info(err);
                }
                connection.release();
            });
            
        }
        
        
        return Action;
    })();
    
    updateCounter = function(sql,cb){
        var action = new Action(sql);
        action.update(function(data,err){
           if(!err){
                cb(data, null);
                
            } else{
                cb(null, data);
            }
        });
        
    }
    
    getEpisodeCounter = function(sql,cb){
        var action = new Action(sql);
        action.episodeCounter(function(data,err){
            
           if(!err){
                cb(data, null);
                
            } else{
                cb(null, data);
            }
        });
        
    }
    
    // get latest 40
    getLatestFourty = function( cb){
        
        pool.getConnection(function(err, connection){
             
                if(!err){ 
                        var langNChSql = "select * from  language "; 
                        var recentlyUpdatedEpisdoeSql = "select l.language_pid, l.name as lang, c.channels_pid, c.name as channel, c.language_fid, cat.categories_pid , cat.name as category, cat.channel_fid,  episode_pid, e.name as epiName, e.description as epDesc, e.createdAt, album_fid, e.counter as epCounter, e.publish, album_pid, a.name, a.imgUrl, a.category_fid from language as l, channels as c, categories as cat, episode as e, album as a where l.language_pid= c.language_fid and c.channels_pid=cat.channel_fid and cat.categories_pid = a.category_fid and album_pid=album_fid order by createdAt desc limit 40;"
                        
                        var lanNChResult = connection.query(langNChSql, function(err, myRows){
                                 
                               
                                if(!err){
                                         
                                        var recentlyUpdateResult = connection.query(recentlyUpdatedEpisdoeSql, function(err, rData){
                                                console.log('data is')
                                                if(!err){
                                                    cb(rData, null);		 
                                                }else{
                                                    cb(  null, rData);
                                                }
                                        });
                                        //logger.info(recentlyUpdateResult.sql);
                                }else{

                                }
                        });


                }else{
                        //logger.info(err);
                }
                connection.release();
        });
        
    }
    
    getLanguages = function(cb){
        pool.getConnection(function(err, connection){
            
            
            if(!err){
                
                var langNChSql = "select * from  language ";  
                var lanNChResult = connection.query(langNChSql, function(err, myRows){
                    console.log('language data');
                    if(!err){
                        cb(myRows, null);		 
                    }else{
                        cb(  null, err);
                    }     
                       
                });
    
    
            }else{
                cb(null,err )
                    //logger.info(err);
            }
            connection.release();
        });
        
    }
    
    getLanguagesDataById = function(langId,cb){
        pool.getConnection(function(err, connection){
            
            
            if(!err){
                
                var langNChSql = "select * from channels where language_fid = " + langId + ";"; 
                var lanNChResult = connection.query(langNChSql, function(err, myRows){
                    console.log('language data');
                    if(!err){
                        cb(myRows, null);		 
                    }else{
                        cb(  null, err);
                    }     
                       
                });
    
    
            }else{
                cb(null,err )
                    //logger.info(err);
            }
            connection.release();
        });
    }
    
    // get albums of the selected  channels
    getAlbumsByChannelsId = function(channelId, cb){
        pool.getConnection(function(err, connection){
             
                if(!err){ 
                        var langNChSql = "select * from categories where channel_fid = ? ;"  ; 
                        //var recentlyUpdatedEpisdoeSql =  " select * from album where category_fid = ? order by name";
                        var recentlyUpdatedEpisodeSql = "select a.album_pid, a.name as albumName, a.description, a.imgUrl, a.category_fid, a.counter, a.publish," +
                        "  c.categories_pid, c.name, c.channel_fid, c.counter from album as a, categories as c where a.category_fid = c.categories_pid and c.channel_fid=? order by a.name asc;";
                        
                        var lanNChResult = connection.query(langNChSql,channelId, function(err, myRows){
                                
                                console.log(myRows[0].channel_fid);
                                if(!err){
                                        
                                        //var recentlyUpdateResult = connection.query(recentlyUpdatedEpisdoeSql, myRows[0].categories_pid, function(err, rData){
                                        console.log("language data");
                                        var recentlyUpdateResult = connection.query(recentlyUpdatedEpisodeSql,myRows[0].channel_fid, function(err, rData){    
                                                  
                                                if(!err){
                                                    cb(rData, null);		 
                                                }else{
                                                    
                                                    cb(  null, rData);
                                                }
                                        });
                                        console.log("language data");
                                        console.log(recentlyUpdateResult.sql)
                                         
                                }else{
                                    cb(  null, myRows);
                                }
                        });


                }else{
                        //logger.info(err);
                }
                connection.release();
        });
    }
    
    //get episodes of the selected album by Id
    getEpisodesByAlbumId = function(albumId,cb){
        pool.getConnection(function(err, connection){
            
            
            if(!err){
                
                var langNChSql = "select * from episode where album_fid =? order by createdAt desc;" ; 
                var lanNChResult = connection.query(langNChSql,albumId, function(err, myRows){
                    console.log('language data');
                    if(!err){
                        cb(myRows, null);		 
                    }else{
                        cb(  null, err);
                    }     
                       
                });
    
    
            }else{
                cb(null,err )
                    //logger.info(err);
            }
            connection.release();
        });
    }
    
    //get links by episode Id
    getLinksByEpisodeId = function(episodeId, cb){
        pool.getConnection(function(err, connection){
            
            
            if(!err){
                
                var langNChSql = "select * from links where episode_fid =? " ; 
                var lanNChResult = connection.query(langNChSql,episodeId, function(err, myRows){
                    console.log('language data');
                    if(!err){
                        cb(myRows, null);		 
                    }else{
                        cb(  null, err);
                    }     
                       
                });
    
    
            }else{
                cb(null,err )
                    //logger.info(err);
            }
            connection.release();
        });
    }
    
    
    //get this week popular albums
    getThisWeekPopularAlbum = function(cb){
        //select * from episode as e, album as a where e.album_fid = a.album_pid and YEARWEEK(e.createdAt, 0) = YEARWEEK(CURDATE(), 0) order by e.counter desc limit 20 ;
    
        pool.getConnection(function(err, connection){
            
            
            if(!err){
                
                var langNChSql = "select * from episode as e, album as a where e.album_fid = a.album_pid and YEARWEEK(e.createdAt, 0) = YEARWEEK(CURDATE(), 0) order by e.counter desc limit 20 ;" ; 
                console.log(langNChSql)
                var lanNChResult = connection.query(langNChSql, function(err, myRows){
                    console.log(err) ;
                
                    if(!err){
                        cb(myRows, null);		 
                    }else{
                        cb(  null, err);
                    }     
                       
                });
    
    
            }else{
                cb(null,err )
                    //logger.info(err);
            }
            connection.release();
        });
    }
    
    root = typeof exports !== 'undefined' && exports !== null ? exports : window;
    root.Action = Action;
    root.getLatestFourty = getLatestFourty;
    root.getLanguages = getLanguages;
    root.getLanguagesDataById = getLanguagesDataById;
    root.getAlbumsByChannelsId = getAlbumsByChannelsId;
    root.getEpisodesByAlbumId = getEpisodesByAlbumId;
    root.getLinksByEpisodeId = getLinksByEpisodeId;
    root.updateCounter = updateCounter;
    root.getEpisodeCounter = getEpisodeCounter;
    root.getThisWeekPopularAlbum = getThisWeekPopularAlbum;
}).call(this);