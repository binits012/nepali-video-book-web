var express = require('express');
var router = express.Router();
var logger = require('../utilities/logger');
var async = require('async');
var Action = require('../routes/actions');


router.get('/v1/latestFourty', function(req, res){

    Action.getLatestFourty(function(data,err){
        if(!err){
            res.send(data); 
        }else{
            logger.info(err);
        }
             
    }); 
});


router.get('/v1/getLanguages', function(req, res){
     
    Action.getLanguages(function(data,err){
        if(!err){
            res.send(data); 
        }else{
            logger.info(err);
        }
             
    }); 
});

router.get('/v1/getLanguages/:langId', function(req, res){
    var id = req.params.langId;
    if (( typeof id != "undefined") && (id != null)) {
        Action.getLanguagesDataById(id,function(data,err){
            if(!err){
                res.send(data); 
            }else{
                logger.info(err);
            }
                 
        }); 
    }
     
});


router.get('/v1/getAlbumsByChannel/:channelId', function(req, res){
    var id = req.params.channelId;
    if (( typeof id != "undefined") && (id != null)) {
        Action.getAlbumsByChannelsId(id,function(data,err){
            if(!err){
                res.send(data); 
            }else{
                logger.info(err);
            }
                 
        }); 
    }
     
});

//get episodes data by Album ID
router.get('/v1/getEpisodesByAlbumId/:albumId', function(req, res){
    var id = req.params.albumId;
    if (( typeof id != "undefined") && (id != null)) {
        Action.getEpisodesByAlbumId(id,function(data,err){
            if(!err){
                res.send(data); 
            }else{
                logger.info(err);
            }
                 
        }); 
    }
     
});

//get links data by episode ID
router.get('/v1/getLinksByEpisodeId/:episodeId', function(req, res){
    var id = req.params.episodeId;
    if (( typeof id != "undefined") && (id != null)) {
        Action.getLinksByEpisodeId(id,function(data,err){
            if(!err){
                res.send(data); 
            }else{
                logger.info(err);
            }
                 
        }); 
    }
     
});


// get this week popular episode
router.get('/v1/getThisWeekPopularAlbums', function(req, res){
     
     
    Action.getThisWeekPopularAlbum(function(data,err){
        if(!err){
            res.send(data); 
        }else{
            logger.info(err);
        }
             
    }); 
 
     
});


module.exports = router;