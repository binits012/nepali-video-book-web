$(document).ready(function(){
	

	$('.login-form').submit(function(e){
		e.preventDefault();
		$('.loginResult').hide('fast');
		$('.loginResultSuccess').hide('fast');
		$.ajax({
			url:'/login',
			DataType:'json',
			type:'post',
			data:$('.login-form').serialize(),
			success:function(data){ 
				if(data != null && typeof (data) != undefined ){
					 if(data.reply === '101'){
					 	$('.loginResult').show('fast');
					 }else if(data.reply === '100') {
					 	$('.loginResultSuccess').show('fast');
					 	location.href = '/dashboard';
					 }else{
					 	location.href = '/login';
					 }
				}
			},
			error:function(request, status, error)
			{
				 
				console.log("status =" + status);
				console.log(request);
				//location.href="/";
				if(request.status == 403){
					location.href = "/login"
				}else{
					
				}
			}
		});
	});



	/* ****** passgenerator add new  user ***/
	$('.passGeneratorAddUserForm').on('submit', function(e){
		e.preventDefault();
		var check = $('.passGeneratorAddUserForm').validate();
		if(check){
			$.ajax({
				url:'/passgenerator',
				dataTye:'json',
				type:'post',
				data:$('.passGeneratorAddUserForm').serialize(),
				success:function(data){
					if(data != null && typeof (data) != undefined ){
						 if(data.reply === '101'){
						  	location.href = '/login';
						 }else if(data.reply === '100') {
						  	location.reload();
						 }else if(data.reply === '105'){
						 	alert("Given user already exists !!");
						 }
					}
				},
				error: function(err){
					alert("Sorry something went wrong. Please try Again !!");
				}
			});
		}
	});

	/* ********************************************** */
	/* ******** passgenerator edit user ************* */
	/* ********************************************** */

	$('.passGenEdit').click( function(e){
		e.preventDefault();
		var i = $(this).attr('id');
		$('.passGenEditModal').modal('show');
		var fname = passData[i].firstName;
		var lname = passData[i].lastName;
		var mobile = passData[i].mobileNumber;
		var id = passData[i].passGenerator_pid;
		 
		$('.passGeneratorModalForm input[name=passGenEditFname]').val(fname);
		$('.passGeneratorModalForm input[name=passGenEditLname]').val(lname);
		$('.passGeneratorModalForm input[name=passGenEditMobile]').val(mobile);
		$('.passGeneratorModalForm input[name=passGeneratorPid]').val(id);
		

	});


	$('.passGeneratorModalForm').on('submit', function(e){
		var check = $('.passGeneratorModalForm').validate();
		if(check ){
			$.ajax({
				url:"/passgenerator/edit",
				dataType:'json',
				data:$('.passGeneratorModalForm').serialize(),
				type:'post',
				success:function(data){
					if(data != null && typeof (data) != undefined ){
						$('.passGenEditModal').modal('hide');
						$('.passGeneratorModalForm')[0].reset();
						 if(data.reply === '101'){
						  	location.href = '/login';
						 }else if(data.reply === '100') {
						  	location.reload();
						 }else if(data.reply === '102'){
						 	alert("Something is quite not wright. Please try again or inform to the engineers !!");
						 }
					}
					
				},
				error: function(err){
					alert(err);
				}
			})
		}
	});


	/* ********************************************** */
	/* ******** Add new managers  ************* */
	/* ********************************************** */

	$('.managerAddUserForm').on('submit', function(e){
		e.preventDefault();
		var check = $('.managerAddUserForm').validate();
		if(check){
			$.ajax({
				url:'/manager',
				dataTye:'json',
				type:'post',
				data:$('.managerAddUserForm').serialize(),
				success:function(data){
					if(data != null && typeof (data) != undefined ){
						 if(data.reply === '101'){
						  	location.href = '/login';
						 }else if(data.reply === '100') {
						  	location.reload();
						 }else if(data.reply === '105'){
						 	alert("Given user already exists !!");
						 }
					}
				},
				error: function(err){
					alert("Sorry something went wrong. Please try Again !!");
				}
			});
		}
	});

	/* ********************************************** */
	/* ******** passgenerator edit user ************* */
	/* ********************************************** */

	$('.managerEdit').click( function(e){
		e.preventDefault();
		var i = $(this).attr('id');
		$('.managerEditModal').modal('show');
		var fname = passData[i].firstName;
		var lname = passData[i].lastName;
		var mobile = passData[i].mobileNumber;
		var id = passData[i].manager_pid;
		 
		$('.managerModalForm input[name=managerEditFname]').val(fname);
		$('.managerModalForm input[name=managerEditLname]').val(lname);
		$('.managerModalForm input[name=managerEditMobile]').val(mobile);
		$('.managerModalForm input[name=managerPid]').val(id);
		

	});


	$('.managerModalForm').on('submit', function(e){
		var check = $('.managerModalForm').validate();
		if(check ){
			$.ajax({
				url:"/manager/edit",
				dataType:'json',
				data:$('.managerModalForm').serialize(),
				type:'post',
				success:function(data){
					if(data != null && typeof (data) != undefined ){
						$('.managerEditModal').modal('hide');
						$('.managerModalForm')[0].reset();
						 if(data.reply === '101'){
						  	location.href = '/login';
						 }else if(data.reply === '100') {
						  	location.reload();
						 }else if(data.reply === '102'){
						 	alert("Something is quite not wright. Please try again or inform to the engineers !!");
						 }
					}
					
				},
				error: function(err){
					alert(err);
				}
			})
		}
	});


});