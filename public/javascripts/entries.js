$(document).ready(function(){

	$('.addlinks').click(function(e){
		e.preventDefault();
		$('.linksTable').hide();
		$('.linksAddForm').show('fast');

	});

	$('.linksAddFormClose').click(function(e){
		e.preventDefault();
		$('.linksTable').show();
		$('.linksAddForm').hide('fast');
	});

	$('.linkEditClass').click(function(e){
		e.preventDefault();
		var i = $(this).attr('id');
		//alert(linkData[i].position);
		$('.linksEditForm input[name=editPosition]').val(linkData[i].position);
		$('.linksEditForm input[name=editLinksUrl]').val(linkData[i].url);
		$('.linksEditForm input[name=linkId]').val(linkData[i].links_pid);
		$('.linksTable').hide();
		$('.linksAddForm').hide();
		$('.linksEditForm').show('fast');

	});

	$('.linksEditFormClose').click(function(e){
		e.preventDefault();
		$('.linksTable').show('fast');
		$('.linksAddForm').hide('fast');
		$('.linksEditForm').hide('fast');
	});


	// adding new item to selected category
	$('.addProgramToChannel').click(function(e){
		e.preventDefault();
		$('.programTable').hide( );
		$('.AddNewProgramForm').show('fast');

	});

	$('.addProgramFormClose').click(function(e){
		e.preventDefault();
		
		$('.AddNewProgramForm').hide( );
		$('.programTable').show('fast');

	});




	// From here it belongs to Program
	$('.albumEditClass').click(function(e){
		e.preventDefault();
		$('.programTable').hide();
		$('.editProgramForm').show('fast');

		var i = $(this).attr('id');
		var program = albumData[i].name;
		var img = albumData[i].imgUrl;
		var description = albumData[i].description;
		var albumId = albumData[i].album_pid; 

		$('.editProgramForm input[name=program]').val(program);
		$('.editProgramForm input[name=img]').val(img);
		$('.editProgramForm input[name=description]').val(description);
		$('.editProgramForm input[name=albumId]').val(albumId);
	});

	$('.editProgramFormClose').click(function(e){
		e.preventDefault();
		$('.programTable').show('fast');
		$('.editProgramForm').hide();
	});
	// program ends here

	// From here it belongs to Add episode on Album
	$('.addAlbum').click(function(e){
		e.preventDefault();
		$('.addNewEpisodeForm').show('fast');
		$('.episodesTable').hide();

	});

	$('.episodeAddFormClose').click(function(e){
		e.preventDefault();
		$('.addNewEpisodeForm').hide();
		$('.episodesTable').show('fast');
		$('.editEpisodeForm').hide();
	});

	$('.episdeEditClass').click(function(e){
		e.preventDefault();
		$('.addNewEpisodeForm').hide();
		$('.episodesTable').hide();
		$('.editEpisodeForm').show('fast');

		var i = $(this).attr('id');
		var episode = episodeData[i].name;
		var description = episodeData[i].description;
		var episodeId = episodeData[i].episode_pid;
		$('.editEpisodeForm input[name=program]').val(episode);
		$('.editEpisodeForm input[name=description]').val(description);
		$('.editEpisodeForm input[name=episodeId]').val(episodeId);


	});

	 // ends here

});