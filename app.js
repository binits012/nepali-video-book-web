var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var cookieSession = require('cookie-session')
var session = require('express-session');

var routes = require('./routes/index');
var users = require('./routes/users');
var smsPost = require('./routes/smsPost');
var dhuwani = require('./routes/dhuwani');
var manager = require('./routes/manager');
var passGenerator = require('./routes/passGenerator');


var api = require('./routes/api');

var entries = require('./routes/entries');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'pug');
app.set('trust proxy', 1) // trust first proxy

// uncomment after placing your favicon in /public
//app.use(favicon(__dirname + '/public/favicon.ico'));
//app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
//app.use(cookieParser());
app.use(cookieParser());

app.use(express.static(path.join(__dirname, 'public')));
app.use(session({
    secret:"dhungaKhani",
  resave: true,rolling:true,
  saveUninitialized: false, cookie: { expires: new Date(Date.now() + 60 * 15000), 
  maxAge: 60*150000 }
}));

app.use('/', routes);
app.use('/users', users);
app.use('/smsPost', smsPost);
app.use('/dhuwani', dhuwani);
app.use('/manager', manager);
app.use('/passGenerator', passGenerator);
app.use('/entries', entries);
app.use('/api', api);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function(err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});
app.set('port', process.env.PORT || 8081);
var server = app.listen(app.get('port'), function() {
  console.log('server is running at = ' + server.address().port);
});




module.exports = app;
