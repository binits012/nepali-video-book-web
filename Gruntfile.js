module.exports = function(grunt){

	grunt.initConfig({
		pkg:grunt.file.readJSON('package.json'),


		copy: {
			main: {
				files: [
				{
					expand: false, 
					src: [ 'node_modules/admin-lte/bootstrap/css/*.css'], 
					dest: 'public/stylesheets/admin-lte-bootstrap.css'
				},
				{
					expand: false, 
					src: [ 'node_modules/admin-lte/dist/css/skins/_all-skin.css'], 
					dest: 'public/stylesheets/admin-lte-skins.css'
				},

				{
					expand: false, 
					src: [ 'node_modules/admin-lte/bootstrap/js/*.js'], 
					dest: 'public/javascripts/admin-lte-bootstrap.js'
				},
				{
					expand: false, 
					src: [ 'node_modules/admin-lte/dist/js/app.js'], 
					dest: 'public/javascripts/admin-lte.js',
				},
				{
					expand: false, 
					src: [ 'node_modules/admin-lte/dist/css/AdminLTE.css'], 
					dest: 'public/stylesheets/admin-lte.css'
				},
				]
			},
		},


		concat:{
			options:{
				separator:'\n\n',
				banner:"'use strict'; \n",
				process:function(src, filepath){
					return '/* Source: '+ filepath + ' */' + '\n' + src.replace(/(^|\n)[ \t]*('use strict'|"use strict");?\s*/g, '$1');

				}, 
			},

			dist:{
				src:['public/javascripts/jquery-*.js', 'public/javascripts/bootstrap.js', 'public/javascripts/validate.js', 'public/javascripts/login.js', 'public/javascripts/player.js' ],
				dest:'public/javascripts/<%= pkg.name %>.js',
			}
		},
		 

		watch:{
			scripts:{
				files: ['public/javascripts/*.js', '!public/javascripts/<%= pkg.name %>.js'],
				tasks: ['concat'],
				options: {
					spawn:false
				}
			}
		},

		uglify:{
			options:{
				banner:'/*! <%= pkg.name %> - <%= grunt.template.today("yyyy-mm-dd") %> */\n'
			},
			build:{
				src:['public/javascripts/<%= pkg.name %>.js'],
				dest:'public/javascripts/<%= pkg.name %>.bundle.js'
			}
		},
		cssmin: {
			options: {
				shorthandCompacting: false,
				roundingPrecision: -1
			},
			target: {
				files: {
					'public/stylesheets/<%= pkg.name %>.css': ['public/stylesheets/*.css']
				}
			}
		},

		// Load the plugin that provides the "uglify" task.
 	


	});

	grunt.loadNpmTasks('grunt-contrib-uglify');

	// Load the plugin that provides the "concat" task.
	grunt.loadNpmTasks('grunt-contrib-concat');

	// Load the plugin that provides the "watch" task.
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Load the plugin that provides the "copy" task.
	grunt.loadNpmTasks('grunt-contrib-copy');

	// Load the plugin for css min during deploy
	grunt.loadNpmTasks('grunt-contrib-cssmin');

	// We want to change some vars in js file while deploy
	//grunt.loadNpmTasks('grunt-string-replace');



	// Default task(s).
	grunt.registerTask('default', ['concat']);

};